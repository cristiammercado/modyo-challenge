
## Contributing

1. [Fork it](https://gitlab.com/cristiammercado/modyo-challenge).
2. Create your feature branch (`git checkout -b my-new-feature`).
3. Commit your changes (`git commit -am 'Added some feature'`).
4. Push to the branch (`git push origin my-new-feature`).
5. Create a new Pull Request.
