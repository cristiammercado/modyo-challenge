# Pokédex #

Application to view a list of Pokémon, in addition to seeing their physical characteristics and other qualities. An integration with API from [https://pokeapi.co/](https://pokeapi.co/) was created to consult the Pokémon information. The application in the backend is built with Spring Boot, and the web application is built with Angular version 11.

This application has been built based on the code challenge for the Modyo company developed by Cristiam Mercado. The main requirements are described in this [form](https://docs.google.com/forms/d/e/1FAIpQLScfc6FJzVmLSZ2RfSUPZ9v0ccuH6xjx-mh_HsbMNkRIQK5zvA/viewform).

- **Current version:** 1.0.2
- **Autor:** Cristiam Mercado Jiménez - <cristiammercado@hotmail.com>

#### **URLs** ####

- **Main:** [https://cm-poke.herokuapp.com](https://cm-poke.herokuapp.com)
- **PokeAPI:** [https://pokeapi.co/](https://pokeapi.co/)
- **Repository:** [https://gitlab.com/cristiammercado/modyo-challenge](https://gitlab.com/cristiammercado/modyo-challenge)

----------
#### **MINIMAL REQUIREMENTS** ####

- Git ([Windows](https://git-scm.com/download/win)) ([Linux](https://www.digitalocean.com/community/tutorials/how-to-install-git-on-ubuntu-20-04))
- Apache Maven 3.8.1 ([Windows](https://maven.apache.org/download.cgi)) ([Linux](https://sdkman.io/sdks#maven))
- Java JDK 11.0.10 ([Windows](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html)) ([Linux](https://www.digitalocean.com/community/tutorials/how-to-install-java-with-apt-on-ubuntu-20-04))

Don't forget to set the system variables:

- `JAVA_HOME` pointing to the JDK installation folder.
- `MAVEN_HOME` & `M2_HOME` pointing to the Maven installation folder.
- Add the `bin` folder of the Maven installation to the operating system path.

If you want to compile the web application (it is already compiled in Spring Boot's resources folder), you must also install the following:

- NodeJS v14.16.1 LTS (includes `npm` 6.14.11) ([Windows](https://nodejs.org/dist/v14.16.1/node-v14.16.1-x64.msi), [Linux](https://github.com/nodesource/distributions/blob/master/README.md))
- Angular CLI ([enlace](https://angular.io/guide/quickstart))

In addition to the above, update once `npm` is installed:

- [npm](https://www.npmjs.com/) (`npm install -g npm@latest`)
- [Angular CLI](https://www.npmjs.com/) (`npm install -g @angular/cli`)

In one line:

```
npm install -g npm @angular/cli
``` 

#### **IINITIAL SETUP** ####

The source code is in a Maven-type project that can be opened with any IDE that supports this type of project (*IntelliJ IDEA* recommended). It is recommended locally you don't have any application running on port 8080 (for Spring Boot API) and 4200 (for web app).

To start coding, remember to run the command `npm install` in a console located in the `frontend` folder of the project. This command will download all the necessary libraries to start the project.

Do the same with the Spring Boot project too, but running the command `mvn clean compile` in the root of the project.

To start coding, remember that [GitFlow](http://nvie.com/posts/a-successful-git-branching-model/) is used (at least for develop and master branches) to handle the branches for `Git`, so make sure it is located in the` develop` branch before making any changes.

Consider installing Apache Maven globally on your operating system (You can use wrapper, too). To find out if it was installed successfully globally, just open a command console and run:

```
mvn -v
```

This should return something like (example in `Windows`):

```
Apache Maven 3.8.1 (05c21c65bdfed0f71a2f2ada8b84da59348c4c5d)
Maven home: C:\Program Files\Maven\bin\..
Java version: 11.0.10, vendor: Oracle Corporation, runtime: C:\Program Files\Java\jdk-11.0.10
Default locale: es_CO, platform encoding: Cp1252
OS name: "windows 10", version: "10.0", arch: "amd64", family: "windows"
```

#### **EXECUTION** ####

To start the Spring Boot applciation, just execute the command:

```
mvn spring-boot:run
```

This command will start the API and the web application on port 8080. With this, you will automatically see the application startup and if there is an error at the time of initialization in your command console.

#### **TESTS** ####

You can run the tests by running the command:

```
mvn clean test
```

You will see in the console output the execution of the tests and the total results.

#### **COMPILATION** ####

To compile the project and generate the executable jar file, you just need to run the command:

```
mvn clean compile package
```

A `.jar` file will be generated in the `target` folder. This file already contains all the built-in dependencies, in such a way that you should not transport or configure an additional library path for its execution.

You can execute the application directly from `.jar` file with the command:

```
java -jar target/modyo-{version}.jar
```

#### **DEPLOY** ####

The project uses the continuous integration functionality provided by GitLab (see `.gitlab-ci.yml` file).

Therefore, when pushing the repository, a task service will be executed according to the branch to which it has been pushed.

Since the project has only two branches, the automated tasks work as follows:

- `develop`: This branch is for development, when the push is done, the application tests are executed.
- `master`: This branch is the main one, the most recent commit of this branch must reflect the current state of the code deployed in the production environment. It is not recommended pushing directly, but through an MR from develop. When closing the MR, a task is executed for application testing. The second task deploys to Heroku and creates the current version tag in the repository.

#### **HEROKU** ####

Heroku is used as the application server. It is free, and the deployment is done through its plugin for Maven. The access token is configured in the CI variables of the repository.

In the `pom.xml` file there is a profile called `production`, where you can view the command used to run the application in a productive environment. The server url is: [https://cm-poke.herokuapp.com](https://cm-poke.herokuapp.com).


#### **CURL / POSTMAN** ####

In the `postman` folder you can find the collections to use the API. It can be imported for both local env and production env. Being endpoints with public information, it does not have any type of authentication.

You can also run the following `curl` commands from the console:

Prod:

- `curl -s -L -m 15 -X GET 'https://cm-poke.herokuapp.com/api/healthcheck'`
- `curl -s -L -m 15 -X GET 'https://cm-poke.herokuapp.com/api/pokemons?page=1&pagesize=10'`
- `curl -s -L -m 15 -X GET 'https://cm-poke.herokuapp.com/api/pokemons/1'`

Local:

- `curl -s -L -m 15 -X GET 'http://127.0.0.1:8080/api/healthcheck'`
- `curl -s -L -m 15 -X GET 'http://127.0.0.1:8080/api/pokemons?page=1&pagesize=10'`
- `curl -s -L -m 15 -X GET 'http://127.0.0.1:8080/api/pokemons/1'`


## Help / Support

If you run into any issues, please email me at [cristiammercado@hotmail.com](mailto:cristiammercado@hotmail.com) or you can use the contact form in [my page](https://cristiammercado.com/en/#contact).

For bug reports, please [open an issue on GitLab](https://gitlab.com/cristiammercado/modyo-challenge/-/issues/new).
