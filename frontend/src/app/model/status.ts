export class Status {

  code: number;
  apiCode: string;
  text: string;
  message: string;

  constructor(code: number, apiCode: string, text: string, message: string) {
    this.code = code;
    this.apiCode = apiCode;
    this.text = text;
    this.message = message;
  }

}
