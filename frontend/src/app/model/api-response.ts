import {Status} from './status';

export class ApiResponse {

  public entity!: any;
  public headers!: Record<string, string | null>;
  public status!: Status;

}
