import {Evolution} from './evolution';

export class Pokemon {

  id!: number;
  name!: string;
  image!: string;
  types!: Array<string>;
  weight!: number;
  abilities!: Array<string>;
  description!: string;
  evolutions!: Array<Evolution>;

}
