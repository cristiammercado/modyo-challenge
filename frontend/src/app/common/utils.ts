import {HttpHeaders} from '@angular/common/http';

export class Utils {

  public static readResponseHeaders(headers: HttpHeaders): Record<string, string | null> {
    const keys: Array<string> = headers.keys();
    const output: Record<string, string | null> = {};

    keys.forEach((value: string, i: number) => {
      output[keys[i]] = headers.get(keys[i]);
    });

    return output;
  }

  public static getMessageFromErrorCode(entity: any): Array<string> {
    const response: Array<string> = [];

    response.push(entity?.error?.code || '1');
    response.push(entity?.error?.message || 'There was an unknown error on the platform');

    return response;
  }

}
