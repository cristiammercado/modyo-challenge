import {Injectable} from '@angular/core';
import {
  HttpClient,
  HttpErrorResponse,
  HttpEvent,
  HttpEventType,
  HttpHeaders,
  HttpParams,
  HttpRequest
} from '@angular/common/http';

import {Observable, throwError} from 'rxjs';
import {catchError, last, map, timeout} from 'rxjs/operators';

import {Utils} from '../common/utils';
import {ApiResponse} from '../model/api-response';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private static TIMEOUT: number = 15000;

  constructor(private http: HttpClient) {
  }

  private static extractData(event: HttpEvent<any>): any {

    if (event.type === HttpEventType.Response) {
      return event.body || {};
    }

  }

  private static handleError(error: HttpErrorResponse): Observable<never> {

    const entity = error.error ? error.error : error;
    const headers = Utils.readResponseHeaders(error.headers ? error.headers : new HttpHeaders());
    const results: Array<string> = Utils.getMessageFromErrorCode(entity);

    const response: ApiResponse = {
      entity,
      headers,
      status: {
        code: error.status,
        apiCode: results[0],
        text: error.statusText,
        message: results[1]
      }
    };

    return throwError(response);
  }

  public get(url: string, params?: HttpParams): Observable<any> {

    const request = new HttpRequest('GET', url, {
      headers: new HttpHeaders({
        'content-type': 'application/json'
      }),
      reportProgress: false,
      params: params || new HttpParams(),
      responseType: 'json'
    });

    return this.http.request(request).pipe(
      timeout(ApiService.TIMEOUT),
      last(),
      map(ApiService.extractData),
      catchError(ApiService.handleError.bind(this))
    );
  }

}
