import {Injectable} from '@angular/core';
import {HttpParams} from '@angular/common/http'

import {Observable} from 'rxjs';

import {ApiService} from './api.service';
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class PokeService {

  constructor(private apiService: ApiService) {

  }

  public list(page: number, pagesize: number): Observable<any> {

    const params: HttpParams = new HttpParams()
      .set('page', page + '')
      .set('pagesize', pagesize + '');

    return this.apiService.get(`${environment.url}/pokemons`, params);
  }

  public get(id: number): Observable<any> {
    return this.apiService.get(`${environment.url}/pokemons/${id}`);
  }

}
