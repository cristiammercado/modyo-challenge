import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogConfig} from '@angular/material/dialog';

import {Pokemon} from '../../model/pokemon';
import {PokeService} from '../../services/poke.service';

@Component({
  selector: 'app-detail-modal',
  templateUrl: './detail-modal.component.html',
  styleUrls: ['./detail-modal.component.scss']
})
export class DetailModalComponent {

  static CONFIG: MatDialogConfig = {
    closeOnNavigation: true,
    disableClose: false,
    hasBackdrop: true,
    width: '95vw',
    maxWidth: 500
  };

  public loading: boolean;
  public error: boolean;
  public pokemon: Pokemon;

  constructor(@Inject(MAT_DIALOG_DATA) private data: any,
              private pokeService: PokeService) {

    this.loading = true;
    this.error = false;
    this.pokemon = this.data.pokemon;

    this.donwloadDetailData(this.pokemon.id);
  }

  public fallbackImg($event: ErrorEvent): void {
    ($event.target as HTMLImageElement).src = '/assets/img/placeholder.png';
  }

  private donwloadDetailData(id: number): void {

    this.pokeService.get(this.pokemon.id)
      .subscribe(
        (response: any) => this.processResponse(response),
        () => this.processError()
      );
  }

  private processResponse(response: any): void {
    if (response.success) {
      this.pokemon = response.data;
      this.loading = false;
    } else {
      this.error = true;
      this.loading = false;
    }
  }

  private processError(): void {
    this.error = true;
    this.loading = false;
  }

}
