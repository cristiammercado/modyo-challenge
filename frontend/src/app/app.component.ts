import {AfterViewInit, Component, HostListener, ViewChild} from '@angular/core';
import {MatPaginator, PageEvent} from '@angular/material/paginator';

import {PokeService} from './services/poke.service';
import {Pokemon} from './model/pokemon';
import {ModalService} from './services/modal.service';
import {DetailModalComponent} from './modals/detail-modal/detail-modal.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit {

  public readonly PAGESIZE: number = 6;

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  public loading: boolean;
  public error: boolean;
  public gridColumns: number;
  public totalLength: number;
  public elements: Array<Pokemon>;

  constructor(private pokeService: PokeService,
              private modalService: ModalService) {

    this.loading = true;
    this.error = false;
    this.gridColumns = 3;
    this.totalLength = 0;
    this.elements = [];

    setTimeout(() => window.document.getElementById('init-preloader')?.remove(), 0);
  }

  ngAfterViewInit(): void {
    this.onResize();
    this.paginator.page.subscribe((evt: PageEvent) => this.loadPage(evt.pageIndex + 1));

    this.loadPage(1);
  }

  @HostListener('window:resize')
  public onResize(): void {
    setTimeout(() => this.gridColumns = (window.innerWidth || 0) < 600 ? 2 : 3, 0);
  }

  public loadPage(page: number): void {
    this.loading = true;
    this.error = false;

    this.pokeService.list(page, this.PAGESIZE)
      .subscribe(
        (response: any) => this.processResponse(response),
        () => this.processError()
      );

  }

  public reload(): void {
    this.paginator.pageIndex = 0;

    this.loadPage(1);
  }

  public fallbackImg($event: ErrorEvent): void {
    ($event.target as HTMLImageElement).src = '/assets/img/placeholder.png';
  }

  public openBasicInfo(pokemon: Pokemon): void {
    this.modalService.open(DetailModalComponent, {pokemon});
  }

  private processResponse(response: any): void {
    if (response.success) {
      const data = response.data;
      this.totalLength = data.total;
      this.elements = data.pokemons;

      this.loading = false;
    } else {
      this.error = true;
      this.loading = false;
    }
  }

  private processError(): void {
    this.error = true;
    this.loading = false;
  }

}
