1.0.1 (2021-04-28)
 * Added tests.
 * Added documentaation in README.
 * Minimal fixes and improvements.

1.0.0 (2021-04-27)
 * First application deployment.
 * Added web application built with Angular 11.
 * Added backend application (API) built with Spring Boot.
