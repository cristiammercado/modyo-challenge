package com.cm.api.controllers;

import com.cm.api.AbstractTest;
import com.cm.api.dto.CheckDTO;
import com.cm.api.dto.ResponseDTO;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Optional;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class HealthCheckCtrlTest extends AbstractTest {

    @Override
    @Before
    public void setUp() {
        super.setUp();
    }

    @Test
    @SuppressWarnings("unchecked")
    public void get() throws Exception {
        String uri = "/api/healthcheck";

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);

        String content = mvcResult.getResponse().getContentAsString();
        ResponseDTO<CheckDTO> responseDTO = mapFromJson(content, ResponseDTO.class, CheckDTO.class);
        assertNotNull(responseDTO);

        Optional<CheckDTO> checkDTO = responseDTO.data();
        assertTrue(checkDTO.isPresent());

        CheckDTO check = checkDTO.get();
        assertNotNull(check);
        assertEquals("API is up, running...", check.info());
    }

}
