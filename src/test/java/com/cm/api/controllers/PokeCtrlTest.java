package com.cm.api.controllers;

import com.cm.api.AbstractTest;
import com.cm.api.dto.PokemonDTO;
import com.cm.api.dto.PokemonListDTO;
import com.cm.api.dto.ResponseDTO;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Optional;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class PokeCtrlTest extends AbstractTest {

    @Override
    @Before
    public void setUp() {
        super.setUp();
    }

    @Test
    @SuppressWarnings("unchecked")
    public void list() throws Exception {
        String uri = "/api/pokemons";

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);

        String content = mvcResult.getResponse().getContentAsString();
        ResponseDTO<PokemonListDTO> responseDTO = mapFromJson(content, ResponseDTO.class, PokemonListDTO.class);
        assertNotNull(responseDTO);

        Optional<PokemonListDTO> pokemonList = responseDTO.data();
        assertTrue(pokemonList.isPresent());

        PokemonListDTO pokemonListDTO = pokemonList.get();
        assertTrue(pokemonListDTO.total() > 0);
        assertTrue(pokemonListDTO.pokemons().size() > 0);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void detail() throws Exception {
        String uri = "/api/pokemons/1";

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);

        String content = mvcResult.getResponse().getContentAsString();
        ResponseDTO<PokemonDTO> responseDTO = mapFromJson(content, ResponseDTO.class, PokemonDTO.class);
        assertNotNull(responseDTO);

        Optional<PokemonDTO> pokemon = responseDTO.data();
        assertTrue(pokemon.isPresent());

        PokemonDTO pokemonDTO = pokemon.get();
        assertTrue(pokemonDTO.id() > 0);
        assertNotNull(pokemonDTO.name());
    }

}
