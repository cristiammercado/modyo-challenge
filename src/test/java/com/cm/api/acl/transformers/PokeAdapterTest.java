package com.cm.api.acl.transformers;


import com.cm.api.dto.PokemonDTO;
import com.cm.api.dto.PokemonListDTO;
import com.cm.api.dto.external.EvolutionChainDTO;
import com.cm.api.dto.external.NamedResourceListDTO;
import com.cm.api.dto.external.PokemonTypeDTO;
import com.cm.api.dto.external.SpecieDTO;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class PokeAdapterTest {

    @Test
    public void fromNamedResourceListToPokemon() throws IOException {
        NamedResourceListDTO namedResourceList = this.readPokemonList();

        PokemonListDTO pokemonListDTO = PokeAdapter.fromNamedResourceListToPokemon(namedResourceList);

        assertEquals(pokemonListDTO.total(), namedResourceList.count());
        assertEquals(pokemonListDTO.pokemons().size(), namedResourceList.results().size());

        PokemonDTO pokemonDTO = pokemonListDTO.pokemons().get(0);

        assertEquals(1, (int) pokemonDTO.id());
        assertEquals(pokemonDTO.name(), "bulbasaur");
        assertNotNull(pokemonDTO.image());
    }

    @Test
    public void fromPokemonTypeToPokemonDetail() throws IOException {

        PokemonTypeDTO pokemonTypeDTO = this.readPokemon();
        SpecieDTO specieDTO = this.readSpecie();
        EvolutionChainDTO evolutionChainDTO = this.readEvolution();

        PokemonDTO pokemonDTO = PokeAdapter.fromPokemonTypeToPokemonDetail(pokemonTypeDTO, specieDTO, evolutionChainDTO);

        assertEquals(pokemonDTO.id(), pokemonTypeDTO.id());
        assertEquals(pokemonDTO.name(), pokemonTypeDTO.name());

        assertTrue(pokemonDTO.types().isPresent());
        assertEquals(pokemonDTO.types().get().size(), pokemonTypeDTO.types().size());

        assertTrue(pokemonDTO.weight().isPresent());
        assertEquals(pokemonDTO.weight().get(), pokemonTypeDTO.weight());

        assertTrue(pokemonDTO.abilities().isPresent());
        assertEquals(pokemonDTO.abilities().get().size(), pokemonTypeDTO.abilities().size());

        assertTrue(pokemonDTO.description().isPresent());

        assertTrue(pokemonDTO.evolutions().isPresent());
        assertTrue(pokemonDTO.evolutions().get().size() > 0);
    }

    public NamedResourceListDTO readPokemonList() throws IOException {
        File file = new File(this.getClass().getClassLoader().getResource("pokemon-list.json").getFile());

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new Jdk8Module());
        return objectMapper.readValue(file, NamedResourceListDTO.class);
    }

    public PokemonTypeDTO readPokemon() throws IOException {
        File file = new File(this.getClass().getClassLoader().getResource("pokemon-detail.json").getFile());

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new Jdk8Module());
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        return objectMapper.readValue(file, PokemonTypeDTO.class);
    }

    public SpecieDTO readSpecie() throws IOException {
        File file = new File(this.getClass().getClassLoader().getResource("pokemon-specie.json").getFile());

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new Jdk8Module());
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        return objectMapper.readValue(file, SpecieDTO.class);
    }

    public EvolutionChainDTO readEvolution() throws IOException {
        File file = new File(this.getClass().getClassLoader().getResource("pokemon-evolution.json").getFile());

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new Jdk8Module());
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        return objectMapper.readValue(file, EvolutionChainDTO.class);
    }

}
