package com.cm.api.common;

import com.cm.api.dto.external.NamedResourceListDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class UtilsTest {

    @Test
    public void getIDFromURL() throws IOException {
        NamedResourceListDTO namedResourceListDTO = this.readPokemonList();

        int id1 = Utils.getIDFromURL(namedResourceListDTO.results().get(0).url());
        int id2 = Utils.getIDFromURL(namedResourceListDTO.results().get(1).url());
        int id3 = Utils.getIDFromURL(namedResourceListDTO.results().get(2).url());
        int id4 = Utils.getIDFromURL("");

        assertEquals(1, id1);
        assertEquals(2, id2);
        assertEquals(3, id3);
        assertEquals(-1, id4);
    }

    @Test
    public void getImageURL() throws IOException {
        NamedResourceListDTO namedResourceListDTO = this.readPokemonList();

        int id1 = Utils.getIDFromURL(namedResourceListDTO.results().get(0).url());
        int id2 = Utils.getIDFromURL(namedResourceListDTO.results().get(1).url());
        int id3 = Utils.getIDFromURL(namedResourceListDTO.results().get(2).url());
        int id4 = Utils.getIDFromURL("");

        String img1 = Utils.getImageURL(id1);
        String img2 = Utils.getImageURL(id2);
        String img3 = Utils.getImageURL(id3);
        String img4 = Utils.getImageURL(id4);

        assertTrue(img1.contains(String.valueOf(id1)));
        assertTrue(img2.contains(String.valueOf(id2)));
        assertTrue(img3.contains(String.valueOf(id3)));
        assertTrue(img4.contains(String.valueOf(id4)));
    }

    public NamedResourceListDTO readPokemonList() throws IOException {
        File file = new File(this.getClass().getClassLoader().getResource("pokemon-list.json").getFile());

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new Jdk8Module());
        return objectMapper.readValue(file, NamedResourceListDTO.class);
    }
}
