package com.cm.api.common;

import java.util.Objects;

/**
 * General utility methods for API.
 */
public class Utils {

    public static int getIDFromURL(String url) {

        if (Objects.isNull(url) || url.isBlank()) {
            return -1;
        }

        String[] split = url.split("/");
        return Integer.parseInt(split[split.length - 1]);
    }

    public static String getImageURL(int id) {
        return String.format("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/%d.png", id);
    }

}
