package com.cm.api.config;

import com.hazelcast.config.Config;
import com.hazelcast.config.MapConfig;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * General API cache configuration with HazelCast.
 */
@Configuration
@EnableCaching
public class CacheConfig {

    @Bean
    Config config() {
        Config config = new Config();

        MapConfig mapConfig = new MapConfig();
        mapConfig.setTimeToLiveSeconds(86400);
        config.getMapConfigs().put("pokemon", mapConfig);

        return config;
    }

}
