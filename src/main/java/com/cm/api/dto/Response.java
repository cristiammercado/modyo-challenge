package com.cm.api.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.serial.Serial;
import org.immutables.value.Value;

import java.util.Optional;

@Value.Immutable
@Serial.Version(1L)
@Value.Style(typeImmutable = "*DTO")
@JsonSerialize(as = ResponseDTO.class)
@JsonDeserialize(as = ResponseDTO.class)
public interface Response<T> {

    Boolean success();

    Optional<T> data();

    Optional<ErrorResponseDTO> error();

}
