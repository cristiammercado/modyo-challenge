package com.cm.api.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.serial.Serial;
import org.immutables.value.Value;

import java.util.List;
import java.util.Optional;

@Value.Immutable
@Serial.Version(1L)
@Value.Style(typeImmutable = "*DTO")
@JsonSerialize(as = PokemonDTO.class)
@JsonDeserialize(as = PokemonDTO.class)
public interface Pokemon {

    Integer id();

    String name();

    String image();

    Optional<List<String>> types();

    Optional<Integer> weight();

    Optional<List<String>> abilities();

    Optional<String> description();

    Optional<List<PokemonEvolutionDTO>> evolutions();

}
