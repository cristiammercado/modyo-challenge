package com.cm.api.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.serial.Serial;
import org.immutables.value.Value;

@Value.Immutable
@Serial.Version(1L)
@Value.Style(typeImmutable = "*DTO")
@JsonSerialize(as = PokemonEvolutionDTO.class)
@JsonDeserialize(as = PokemonEvolutionDTO.class)
public interface PokemonEvolution {

    Integer id();

    String name();

}
