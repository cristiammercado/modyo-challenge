package com.cm.api.dto.external;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.serial.Serial;
import org.immutables.value.Value;

import java.util.List;

@Value.Immutable
@Serial.Version(1L)
@Value.Style(typeImmutable = "*DTO")
@JsonSerialize(as = PokemonTypeDTO.class)
@JsonDeserialize(as = PokemonTypeDTO.class)
public interface PokemonType {

    Integer id();

    Integer weight();

    String name();

    List<TypeListDTO> types();

    List<AbilityListDTO> abilities();

}
