package com.cm.api.dto.external;

import com.cm.api.common.Utils;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.serial.Serial;
import org.immutables.value.Value;

import java.util.List;

@Value.Immutable
@Serial.Version(1L)
@Value.Style(typeImmutable = "*DTO")
@JsonSerialize(as = SpecieDTO.class)
@JsonDeserialize(as = SpecieDTO.class)
public interface Specie {

    @JsonProperty("evolution_chain")
    EvolutionUrlDTO evolutionChain();


    @JsonProperty("flavor_text_entries")
    List<FlavorTextEntriesDTO> flavorTextEntries();

    @Value.Default
    default int evolutionChainID() {
        return Utils.getIDFromURL(this.evolutionChain().url());
    }

}
