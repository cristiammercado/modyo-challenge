package com.cm.api.dto.external;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.serial.Serial;
import org.immutables.value.Value;

import java.util.List;
import java.util.Optional;

@Value.Immutable
@Serial.Version(1L)
@Value.Style(typeImmutable = "*DTO")
@JsonSerialize(as = NamedResourceListDTO.class)
@JsonDeserialize(as = NamedResourceListDTO.class)
public interface NamedResourceList {

    Integer count();

    Optional<String> next();

    Optional<String> previous();

    List<NamedResourceDTO> results();

}
