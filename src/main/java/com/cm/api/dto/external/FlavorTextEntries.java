package com.cm.api.dto.external;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.serial.Serial;
import org.immutables.value.Value;

@Value.Immutable
@Serial.Version(1L)
@Value.Style(typeImmutable = "*DTO")
@JsonSerialize(as = FlavorTextEntriesDTO.class)
@JsonDeserialize(as = FlavorTextEntriesDTO.class)
public interface FlavorTextEntries {

    @JsonProperty("flavor_text")
    String flavorText();

    NamedResourceDTO language();

    NamedResourceDTO version();

}
