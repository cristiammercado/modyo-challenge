package com.cm.api.dto.external;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.serial.Serial;
import org.immutables.value.Value;

@Value.Immutable
@Serial.Version(1L)
@Value.Style(typeImmutable = "*DTO")
@JsonSerialize(as = EvolutionUrlDTO.class)
@JsonDeserialize(as = EvolutionUrlDTO.class)
public interface EvolutionUrl {

    String url();

}
