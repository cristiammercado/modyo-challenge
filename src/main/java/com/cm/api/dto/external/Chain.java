package com.cm.api.dto.external;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.serial.Serial;
import org.immutables.value.Value;

import java.util.List;

@Value.Immutable
@Serial.Version(1L)
@Value.Style(typeImmutable = "*DTO")
@JsonSerialize(as = ChainDTO.class)
@JsonDeserialize(as = ChainDTO.class)
public interface Chain {

    @JsonProperty("evolves_to")
    List<ChainDTO> evolvesTo();

    NamedResourceDTO species();

}
