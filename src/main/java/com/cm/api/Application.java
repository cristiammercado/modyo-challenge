package com.cm.api;

import com.cm.api.config.WebConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.openfeign.EnableFeignClients;

import java.util.TimeZone;

/**
 * Application startup class.
 */
@SpringBootApplication
@EnableFeignClients
@EnableCaching
public class Application {

    public static void main(String[] args) {
        TimeZone.setDefault(TimeZone.getTimeZone(WebConfig.APP_TIMEZONE));
        SpringApplication.run(Application.class, args);
    }

}
