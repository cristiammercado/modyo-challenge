package com.cm.api.acl.clients;

import com.cm.api.dto.external.EvolutionChainDTO;
import com.cm.api.dto.external.NamedResourceListDTO;
import com.cm.api.dto.external.PokemonTypeDTO;
import com.cm.api.dto.external.SpecieDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Feign client to make requests to Poke API.
 */
@RequestMapping
@FeignClient(name = "pokeapi", url = "${pokeapi.url}")
public interface PokeAPIPokemon {

    @GetMapping("/pokemon")
    NamedResourceListDTO list(@RequestParam("offset") Integer offset, @RequestParam("limit") Integer limit);

    @GetMapping("/pokemon/{id}")
    PokemonTypeDTO read(@PathVariable("id") Integer id);

    @GetMapping("/pokemon-species/{id}")
    SpecieDTO species(@PathVariable("id") Integer id);

    @GetMapping("/evolution-chain/{id}")
    EvolutionChainDTO evolutionChain(@PathVariable("id") Integer id);

}
