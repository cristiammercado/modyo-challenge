package com.cm.api.acl.transformers;

import com.cm.api.common.Utils;
import com.cm.api.dto.PokemonDTO;
import com.cm.api.dto.PokemonEvolutionDTO;
import com.cm.api.dto.PokemonListDTO;
import com.cm.api.dto.external.ChainDTO;
import com.cm.api.dto.external.EvolutionChainDTO;
import com.cm.api.dto.external.NamedResourceListDTO;
import com.cm.api.dto.external.PokemonTypeDTO;
import com.cm.api.dto.external.SpecieDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Adapter to transform entities from external Poke API to internal API data transfer objects.
 */
public class PokeAdapter {

    public static PokemonListDTO fromNamedResourceListToPokemon(NamedResourceListDTO namedResourceList) {
        List<PokemonDTO> pokemons = new ArrayList<>();

        if (Objects.isNull(namedResourceList) || namedResourceList.results().size() < 1) {
            return PokemonListDTO.builder()
                .total(0)
                .pokemons(pokemons)
                .build();
        }

        namedResourceList.results().forEach(e -> {
            int id = Utils.getIDFromURL(e.url());
            String image = Utils.getImageURL(id);

            PokemonDTO dto = PokemonDTO.builder()
                .id(id)
                .name(e.name())
                .image(image)
                .build();

            pokemons.add(dto);
        });

        return PokemonListDTO.builder()
            .total(namedResourceList.count())
            .pokemons(pokemons)
            .build();
    }

    public static PokemonDTO fromPokemonTypeToPokemonDetail(PokemonTypeDTO pokemon, SpecieDTO species, EvolutionChainDTO evolutionChain) {
        String image = Utils.getImageURL(pokemon.id());

        List<String> types = new ArrayList<>();
        List<String> abilities = new ArrayList<>();
        List<String> descriptions = new ArrayList<>();

        pokemon.types().forEach(t -> types.add(t.type().name()));
        pokemon.abilities().forEach(a -> abilities.add(a.ability().name()));

        species.flavorTextEntries()
            .stream()
            .filter(f -> f.language().name().equals("en"))
            .map(f -> f.flavorText().replace("\n", " ").replace("\r", " ").replace("\f", " "))
            .forEach(descriptions::add);

        List<PokemonEvolutionDTO> evolutions = PokeAdapter.exploreChilds(evolutionChain.chain(), new ArrayList<>());

        return PokemonDTO.builder()
            .id(pokemon.id())
            .name(pokemon.name())
            .image(image)
            .types(types)
            .weight(pokemon.weight())
            .abilities(abilities)
            .description(String.join(" ", descriptions))
            .evolutions(evolutions)
            .build();
    }

    private static List<PokemonEvolutionDTO> exploreChilds(ChainDTO parent, List<PokemonEvolutionDTO> childs) {
        int id = Utils.getIDFromURL(parent.species().url());

        PokemonEvolutionDTO child = PokemonEvolutionDTO.builder()
            .id(id)
            .name(parent.species().name())
            .build();

        childs.add(child);

        if (parent.evolvesTo().size() > 0) {
            parent.evolvesTo().forEach(c -> PokeAdapter.exploreChilds(c, childs));
        }

        return childs;
    }

}
