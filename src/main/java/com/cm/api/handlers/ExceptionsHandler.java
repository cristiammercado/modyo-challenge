package com.cm.api.handlers;

import com.cm.api.dto.ErrorResponseDTO;
import com.cm.api.dto.ResponseDTO;
import com.cm.api.exceptions.ErrorMessages;
import feign.FeignException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Optional;

/**
 * General API handler for exceptions.
 */
@ControllerAdvice
public class ExceptionsHandler extends ResponseEntityExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionsHandler.class);

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
        LOGGER.error("Error in internal exception handler", ex);

        ErrorResponseDTO error = ErrorResponseDTO.builder()
            .code(status.value())
            .message(ErrorMessages.getByCode(status.value()).getMessage())
            .build();

        ResponseDTO<Object> response = ResponseDTO.builder()
            .success(false)
            .error(Optional.of(error))
            .build();

        return new ResponseEntity<>(response, headers, status);
    }

    @ExceptionHandler(Throwable.class)
    protected ResponseEntity<Object> handleException(Throwable ex) {
        LOGGER.error("Error in exception handler", ex);

        ErrorResponseDTO error;

        if (ex instanceof FeignException) {
            FeignException fe = (FeignException) ex;

            error = ErrorResponseDTO.builder()
                .code(ErrorMessages.getByCode(fe.status()).getCode())
                .message(ErrorMessages.getByCode(fe.status()).getMessage())
                .build();
        } else {
            error = ErrorResponseDTO.builder()
                .code(ErrorMessages.INTERNAL_SERVER_ERROR.getCode())
                .message(ErrorMessages.INTERNAL_SERVER_ERROR.getMessage())
                .build();
        }

        ResponseDTO<Object> response = ResponseDTO.builder()
            .success(false)
            .error(Optional.of(error))
            .build();

        return new ResponseEntity<>(response, HttpStatus.valueOf(error.code()));
    }

}
