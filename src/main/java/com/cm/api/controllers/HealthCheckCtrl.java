package com.cm.api.controllers;

import com.cm.api.dto.CheckDTO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller for API health check routes.
 */
@RestController
public class HealthCheckCtrl {

    @GetMapping(value = "/api/healthcheck")
    public CheckDTO get() {
        return CheckDTO.builder().info("API is up, running...").build();
    }

}
