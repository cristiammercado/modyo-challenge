package com.cm.api.controllers;

import com.cm.api.dto.PokemonDTO;
import com.cm.api.dto.PokemonListDTO;
import com.cm.api.services.PokemonService;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutionException;

import static com.cm.api.config.WebConfig.DEFAULT_PAGE;
import static com.cm.api.config.WebConfig.DEFAULT_PAGESIZE;

/**
 * Controller for API pokemon routes.
 */
@RestController
@RequestMapping(value = "/api/pokemons")
public class PokeCtrl {

    private final PokemonService pokemonService;

    public PokeCtrl(PokemonService pokemonService) {
        this.pokemonService = pokemonService;
    }

    @Cacheable("pokemons")
    @GetMapping(value = "")
    public PokemonListDTO list(@RequestParam(defaultValue = DEFAULT_PAGE) Integer page, @RequestParam(defaultValue = DEFAULT_PAGESIZE) Integer pagesize) throws ExecutionException, InterruptedException {
        return this.pokemonService.list(page, pagesize);
    }

    @Cacheable("pokemons-detail")
    @GetMapping(value = "/{id}")
    public PokemonDTO detail(@PathVariable("id") Integer id) {
        return this.pokemonService.detail(id);
    }

}
