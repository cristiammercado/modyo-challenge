package com.cm.api.exceptions;

import java.util.Arrays;
import java.util.Objects;

/**
 * Error messages list.
 */
public enum ErrorMessages {

    INTERNAL_SERVER_ERROR(500, "An error has ocurred in the application. Check the logs for more information."),

    BAD_REQUEST(400, "Bad request, please check and try again"),

    NOT_FOUND(404, "Resource not found");

    private final Integer code;

    private final String message;

    ErrorMessages(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public static ErrorMessages getByCode(Integer value) {

        return Arrays.stream(ErrorMessages.values())
            .filter(e -> Objects.equals(e.code, value))
            .findFirst()
            .orElse(ErrorMessages.INTERNAL_SERVER_ERROR);
    }

}
