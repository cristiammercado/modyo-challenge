package com.cm.api.services;

import com.cm.api.dto.PokemonDTO;
import com.cm.api.dto.external.NamedResourceListDTO;
import com.cm.api.dto.external.PokemonTypeDTO;
import com.cm.api.dto.external.SpecieDTO;

/**
 * Poke API service.
 */
public interface PokeAPIService {

    NamedResourceListDTO list(int page, int pageSize);

    PokemonTypeDTO read(int id);

    SpecieDTO specie(int id);

    PokemonDTO evolutionChain(int id);

}
