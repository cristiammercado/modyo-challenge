package com.cm.api.services.impl;

import com.cm.api.acl.clients.PokeAPIPokemon;
import com.cm.api.acl.transformers.PokeAdapter;
import com.cm.api.dto.PokemonDTO;
import com.cm.api.dto.PokemonListDTO;
import com.cm.api.dto.external.EvolutionChainDTO;
import com.cm.api.dto.external.NamedResourceListDTO;
import com.cm.api.dto.external.PokemonTypeDTO;
import com.cm.api.dto.external.SpecieDTO;
import com.cm.api.services.PokemonService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

/**
 * Pokemon service implementation.
 */
@Service
public class PokemonServiceImpl implements PokemonService {

    private final PokeAPIPokemon pokeAPIPokemon;

    public PokemonServiceImpl(PokeAPIPokemon pokeAPIPokemon) {
        this.pokeAPIPokemon = pokeAPIPokemon;
    }

    @Override
    public PokemonListDTO list(int page, int pageSize) throws ExecutionException, InterruptedException {
        NamedResourceListDTO pokemons = this.pokeAPIPokemon.list((page - 1) * pageSize, pageSize);
        PokemonListDTO list = PokeAdapter.fromNamedResourceListToPokemon(pokemons);

        List<PokemonDTO> newList = list.pokemons().stream().parallel().map(p -> {
            PokemonTypeDTO pt = this.pokeAPIPokemon.read(p.id());

            List<String> types = pt.types().stream().map(tl -> tl.type().name()).collect(Collectors.toList());
            List<String> abilities = pt.abilities().stream().map(tl -> tl.ability().name()).collect(Collectors.toList());

            return p.withWeight(pt.weight()).withTypes(types).withAbilities(abilities);
        }).collect(Collectors.toList());

        return list.withPokemons(newList);
    }

    @Override
    public PokemonDTO detail(int id) {
        PokemonTypeDTO pokemon = this.pokeAPIPokemon.read(id);
        SpecieDTO species = this.pokeAPIPokemon.species(id);
        EvolutionChainDTO evolutionChain = this.pokeAPIPokemon.evolutionChain(species.evolutionChainID());

        return PokeAdapter.fromPokemonTypeToPokemonDetail(pokemon, species, evolutionChain);
    }


}
