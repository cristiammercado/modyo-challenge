package com.cm.api.services.impl;

import com.cm.api.acl.clients.PokeAPIPokemon;
import com.cm.api.dto.PokemonDTO;
import com.cm.api.dto.external.NamedResourceListDTO;
import com.cm.api.dto.external.PokemonTypeDTO;
import com.cm.api.dto.external.SpecieDTO;
import com.cm.api.services.PokeAPIService;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * Poke API service.
 */
@Service
public class PokeAPIServiceImpl implements PokeAPIService {

    private final PokeAPIPokemon pokeAPIPokemon;

    public PokeAPIServiceImpl(PokeAPIPokemon pokeAPIPokemon) {
        this.pokeAPIPokemon = pokeAPIPokemon;
    }

    @Override
    @Cacheable("feign")
    public NamedResourceListDTO list(int page, int pageSize) {
        return this.pokeAPIPokemon.list((page - 1) * pageSize, pageSize);
    }

    @Override
    @Cacheable("feign")
    public PokemonTypeDTO read(int id) {
        return this.pokeAPIPokemon.read(id);
    }

    @Override
    @Cacheable("feign")
    public PokemonDTO evolutionChain(int id) {
        return null;
    }

    @Override
    @Cacheable("feign")
    public SpecieDTO specie(int id) {
        return this.pokeAPIPokemon.species(id);
    }

}
