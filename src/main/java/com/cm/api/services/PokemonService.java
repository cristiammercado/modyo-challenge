package com.cm.api.services;

import com.cm.api.dto.PokemonDTO;
import com.cm.api.dto.PokemonListDTO;

import java.util.concurrent.ExecutionException;

/**
 * Pokemon service.
 */
public interface PokemonService {

    PokemonListDTO list(int page, int pageSize) throws ExecutionException, InterruptedException;

    PokemonDTO detail(int id);

}
